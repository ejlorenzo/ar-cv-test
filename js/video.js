var greenFace = false;
var maskFace = true;

(function() {

  var canvas = document.querySelector('canvas');
  var context = canvas.getContext('2d');
  var video = document.querySelector('video');
  var vendorUrl = window.URL || window.webkitURL;
  var localMediaStream = null;

  var tracker = new tracking.ObjectTracker('face');
  tracker.setInitialScale(2);
  tracker.setStepSize(4);
  tracker.setEdgesDensity(0.1);

  var trackerTask = tracking.track('#video', tracker, { camera: true });
  
  tracker.on('track', function(event) {
    setTimeout(function() { trackerTask.stop() }, 1)
    // Clear all rectangles on screen
    context.clearRect(0, 0, canvas.width, canvas.height);
    draw(event);

    setTimeout(function() { trackerTask.run() }, 10);

    // event.data.forEach((rect) => {
    //   context.strokeStyle = '#a64ceb';
    //   context.strokeRect(rect.x, rect.y, rect.width, rect.height);
    //   context.font = '11px Helvetica';
    //   context.fillStyle = "#fff";
    //   context.fillText('x: ' + rect.x + 'px', rect.x + rect.width + 5, rect.y + 11);
    //   context.fillText('y: ' + rect.y + 'px', rect.x + rect.width + 5, rect.y + 22);
    // });
  })

  navigator.getMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

  navigator.getMedia({
    video: true,
    audio: false
  }, function(stream) {
    video.src = vendorUrl.createObjectURL(stream);
    localMediaStream = stream;
    video.play();
  }, function(error) {
    console.error('There has been an error:', error)
  })

  video.addEventListener('play', function() {
    draw();
  }, false)

  function draw(event) {
    context.drawImage(video, 0, 0, canvas.width, canvas.height)

    if (event) {
      if (event.data.length > 0) {
        let face = event.data[0];

        if (greenFace) {
          // Get the array of all pixels inside the face rectangle
          //let data = context.getImageData(face.x, face.y, face.width, face.height).data;
          let data = context.getImageData(face.x + Math.floor(face.width / 2), face.y + Math.floor(face.height / 2), 1, 1).data;

          // Iterate them to find out the average color
          let averageColor = [data[0], data[1], data[2]];

          // for (let i = 0; i < data.length; i += 4) {
          //   averageColor = [
          //     (averageColor[0] + data[i]) / 2,
          //     (averageColor[1] + data[i+1]) / 2,
          //     (averageColor[2] + data[i+2]) / 2
          //   ]
          // }

          // Color threshold to apply the green overlay
          let threshold = 30;

          for (let y = face.y; y < face.y + face.height; y++) {
            for (let x = face.x; x < face.x + face.width; x++) {
              let rgba = context.getImageData(x, y, 1, 1).data;

              if (compareColors(rgba, averageColor, threshold)) {
                context.strokeStyle = 'rgba(0, 255, 0, 0.1)';
                context.strokeRect(x, y, 1, 1);
              }
            }
          }

          snapshot()
        } // end green face

        if (maskFace) {
          let img = document.getElementById('hidden');
          context.drawImage(img, face.x + Math.floor(face.width / 2) - Math.floor(img.width / 2), face.y + Math.floor(face.height / 3) - Math.floor(img.height / 2))
        } // end mask face
      }
    }

    //setTimeout(draw, 10)
  }

  function snapshot() {
    if (localMediaStream) {
      //context.drawImage(video, 0, 0);
      // "image/webp" works in Chrome.
      // Other browsers will fall back to image/png.
      document.getElementById('snapshot').src = canvas.toDataURL('image/webp');
    }
  }

  canvas.addEventListener('click', snapshot, false);

  function rgbToHex(r, g, b) {
    if (r > 255 || g > 255 || b > 255)
      throw "Invalid color component";
    return ((r << 16) | (g << 8) | b).toString(16);
  }

  function scaleRGB(rgb, scale) {
    let r = rgb[0] * scale[0] <= 255 ? rgb[0] * scale[0] : 255;
    let g = rgb[1] * scale[1] <= 255 ? rgb[1] * scale[1] : 255;
    let b = rgb[2] * scale[2] <= 255 ? rgb[2] * scale[2] : 255;

    return [r, g, b];
  }

  function compareColors(c1, c2, threshold) {
    return (
      // Red
      (c1[0] >= c2[0] - threshold && c1[0] <= c2[0] + threshold) &&
      // Green
      (c1[1] >= c2[1] - threshold && c1[1] <= c2[1] + threshold) &&
      // Blue
      (c1[2] >= c2[2] - threshold && c1[2] <= c2[2] + threshold)
    );
  }
})();